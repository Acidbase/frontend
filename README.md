A simple web-application to facilitate the following three functionalities
within Acidbase project:

1. Entity & index structure definition
2. CRUD entities
3. List indices

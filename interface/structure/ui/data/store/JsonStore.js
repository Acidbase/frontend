Ext.define('Acidbase.data.store.JsonStore', {
    extend: 'Ext.data.JsonStore',
    constructor: function() {
        var me = this;
        me.callParent(arguments);
        if (me.remoteListener) {
            /*
             Ab.subscribe(me.remoteListener, function() {
                me.reload();
            });*/
            //Ab.unsubscribe(me.remoteListener, me.reload, me);
        }
    },

    clearListeners: function() {
        var me = this;
        me.callParent(arguments);
    },
    destroyStore: function() {
        var me = this;
        me.callParent(arguments);
    }
});

/*
 * File: ui/model/structure.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Acidbase.model.structure', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'index',
            type: 'boolean'
        },
        {
            name: 'script_name',
            type: 'string'
        },
        {
            name: 'version',
            type: 'int'
        },
        {
            convert: function(v, rec) {
                if (typeof v == "undefined") {
                    return rec.get("name");
                }
                else {
                    return v;
                }
            },
            serialize: function(value, record) {
                var id = value + "";
                record.set("id", record.get("name"));
                return id;
            },
            name: 'id',
            type: 'string'
        },
        {
            name: 'readingStructure',
            type: 'auto'
        },
        {
            convert: function(v, rec) {
                if (v instanceof Object && !(v instanceof Array) && typeof v.iconCls == "undefined") {
                    var c = function(src, des) {
                        des["loaded"] = true;
                        des["revision_state"] = src.revision_state;
                        des["entity_type"] = src.entity_type;
                        des["iconCls"] = "collection";

                        var hasGotField = src.fields && JSON.stringify(src.fields) !== "{}";
                        var hasGotCollections = src.collections && JSON.stringify(src.collections) !== "{}";

                        var fields = [];
                        if (hasGotField) {
                            for (var index in src.fields) {
                                fields.push({
                                    name: index,
                                    field_type: src.fields[index],
                                    loaded: true,
                                    leaf: true,
                                    iconCls: "field"
                                });
                            }
                        }
                        des["children"] = [ { name: "fields", loaded: true, leaf: false, children: fields, expanded: true, iconCls: "constant" } ];

                        var collections = [];
                        if (hasGotCollections) {
                            for (var index in src.collections) {
                                var node = { name: index, leaf: false };
                                c(src.collections[index], node);
                                collections.push(node);
                            }
                        }
                        des["children"].push({ name: "collections", loaded: true, leaf: false, children: collections, expanded: true, iconCls: "constant" });

                        des["expanded"] = true;
                    };

                    var tree = {};
                    c(v, tree);

                    tree["iconCls"] = "root";

                    return tree;
                }
                else {
                    return v;
                }
            },
            serialize: function(value, record) {
                var readingStructure = {};
                var writingStructure = {};

                var r = function(src, des) {
                    des["revision_state"] = src.revision_state;
                    des["entity_type"] = src.entity_type;

                    var fieldsNode = null;
                    var collectionsNode = null;

                    if (src.children && src.children.length > 0) {
                        for (var index in src.children) {
                            if (src.children[index].name == "fields") {
                                fieldsNode = src.children[index].children;
                            }
                            else if (src.children[index].name == "collections") {
                                collectionsNode = src.children[index].children;
                            }
                        }
                    }

                    var fields = [];
                    if (fieldsNode) {
                        for (var index in fieldsNode) {
                            fields.push(fieldsNode[index].name);
                        }
                    }
                    des["fields"] = fields;

                    if (collectionsNode) {
                        var collections = {};
                        for (var index in collectionsNode) {
                            var node = {};
                            r(collectionsNode[index], node);
                            collections[collectionsNode[index].name] = node;
                        }
                        des["collections"] = collections;
                    }
                };

                var w = function(src, des) {
                    des["revision_state"] = src.revision_state;
                    des["entity_type"] = src.entity_type;

                    var fieldsNode = null;
                    var collectionsNode = null;

                    if (src.children && src.children.length > 0) {
                        for (var index in src.children) {
                            if (src.children[index].name == "fields") {
                                fieldsNode = src.children[index].children;
                            }
                            else if (src.children[index].name == "collections") {
                                collectionsNode = src.children[index].children;
                            }
                        }
                    }

                    var fields = {};
                    if (fieldsNode) {
                        for (var index in fieldsNode) {
                            fields[fieldsNode[index].name] = fieldsNode[index].field_type;
                        }
                    }
                    des["fields"] = fields;

                    if (collectionsNode) {
                        var collections = {};
                        for (var index in collectionsNode) {
                            var node = {};
                            w(collectionsNode[index], node);
                            collections[collectionsNode[index].name] = node;
                        }
                        des["collections"] = collections;
                    }
                };

                r(value, readingStructure);
                w(value, writingStructure);

                record.set("readingStructure", readingStructure);

                return writingStructure;

            },
            name: 'writingStructure',
            type: 'auto'
        }
    ]
});
/*
 * File: ui/model/entity.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Acidbase.model.entity', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'name',
            type: 'string'
        },
        {
            convert: function(v, rec) {
                if (typeof v == "undefined") {
                    return rec.get("name");
                }
                else {
                    return v;
                }
            },
            serialize: function(value, record) {
                var id = value + "";
                record.set("id", record.get("name"));
                return id;
            },
            name: 'id',
            type: 'string'
        },
        {
            convert: function(v, rec) {
                if (typeof v == "string" && v != "") {
                    var arr = [];
                    var obj = JSON.parse(v);
                    for (var index in obj) {
                        arr.push({
                            name: index,
                            type: obj[index]
                        });
                    }

                    return arr;
                }
                else {
                    return v;
                }
            },
            serialize: function(value, record) {
                var fields = {};
                for (var index=0; index<value.length; index++) {
                    fields[value[index].name] = value[index].type;
                }
                return JSON.stringify(fields);
            },
            name: 'fields',
            type: 'auto'
        }
    ]
});
Ext.define('Acidbase.data.proxy.AbProxy', {
    extend: 'Ext.data.proxy.Ajax',
    alias : 'proxy.ab-proxy',

    constructor: function(config) {
        var me = this;
        me.callParent([config]);
    },

    doRequest: function(operation, callback, scope) {
        if (!operation)
            return;

        var me = this,
            reader,
            result;

        var processResponse = function(response) {
            if (response.status.success === true) {
                reader = me.getReader();
                result = reader.read(me.extractResponseData(response.data));
                Ext.apply(operation, {
                    response: response,
                    resultSet: result
                });
                operation.commitRecords(result.records);
                operation.setCompleted();
                operation.setSuccessful();
            }
            else {
                me.setException(operation, { error: true });
                me.fireEvent('exception', this, { error: true }, operation, response.status.message);
            }

            //this callback is the one that was passed to the 'read' or 'write' function above
            if (typeof callback == 'function') {
                callback.call(scope, operation);
            }
        };

        var from = (operation.page - 1) * operation.limit;
        var count = operation.limit;
        var params = {};
        scope.filters.eachKey(function(key, value) {
            if (value.initialConfig) {
                params[value.initialConfig.property] = value.initialConfig.value;
            }
            else {
                params[key] = value;
            }
        });

        var sortColumn = null;
        var sortDirection = null;
        if (operation.sorters.length > 0) {
            sortColumn = operation.sorters[0].property;
            sortDirection = operation.sorters[0].direction == "DESC" ? "desending" : "ascending";
        }

        Pome.model({
            class_path: scope.proxy.url,
            method: scope.proxy.method,
            params: [from, count, sortColumn, sortDirection, params],
            callback: processResponse
        });
    }
});
/*
 * File: ui/view/SelectEntityType.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Acidbase.view.SelectEntityType', {
    extend: 'Ext.window.Window',

    height: 250,
    width: 275,
    layout: {
        type: 'fit'
    },
    title: 'Select entity\'s type',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    title: '',
                    hideHeaders: true,
                    store: 'EntityTypeStore',
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'name',
                            text: 'String',
                            flex: 1
                        }
                    ],
                    listeners: {
                        selectionchange: {
                            fn: me.onGridpanelSelectionChange,
                            scope: me
                        },
                        itemdblclick: {
                            fn: me.onGridpanelItemDblClick,
                            scope: me
                        }
                    }
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    layout: {
                        pack: 'end',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            width: 70,
                            text: 'Cancel',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            disabled: true,
                            itemId: 'btn-ok',
                            width: 70,
                            text: 'Ok',
                            listeners: {
                                click: {
                                    fn: me.onOk,
                                    scope: me
                                }
                            }
                        }
                    ]
                }
            ],
            listeners: {
                show: {
                    fn: me.onWindowShow,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onGridpanelSelectionChange: function(model, selected, eOpts) {
        if (selected.length > 0) {
            this.entity = selected[0];
            this.down("[itemId=btn-ok]").enable();
        }
        else {
            this.entity = null;
            this.down("[itemId=btn-ok]").disable();
        }
    },

    onGridpanelItemDblClick: function(dataview, record, item, index, e, eOpts) {
        this.down("[itemId=btn-ok]").fireEvent("click");
    },

    onButtonClick: function(button, e, eOpts) {
        this.destroy();
    },

    onOk: function(button, e, eOpts) {
        var store = Ext.getStore("EntityStore");
        var justAdded = store.add({
            entity_id: null,
            revision_id: null,
            entity_type: this.entity.get("name"),
            create_date: new Date(),
            revise_date: new Date(),
            creator: null,
            reviser: null,
            revision_state: "",
            fields: {},
            data: {}
        });

        var w = Ext.ComponentQuery.query("[itemId=view-main]")[0];
        w.down("[itemId=btn-new-entity]").disable();

        var fields = {};
        for (var index in this.entity.get("fields")) {
            fields[index] = null;
        }

        w.singleEntity = Ext.create('Acidbase.model.entity', {
            entity_id: null,
            revision_id: null,
            entity_type: this.entity.get("name"),
            revision_state: "Active",
            create_date: 0,
            revise_date: 0,
            fields: fields,
            collections: {}
        });
        w.singleEntity.setDirty();

        w.down("[itemId=grid-entities]").getSelectionModel().select(justAdded);

        this.destroy();
    },

    onWindowShow: function(component, eOpts) {
        Ext.getStore("EntityTypeStore").reload();
    }

});
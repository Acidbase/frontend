/*
 * File: ui/view/Progress.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Acidbase.view.Progress', {
    extend: 'Ext.window.Window',

    height: 135,
    width: 451,
    resizable: false,
    layout: {
        align: 'stretch',
        padding: 20,
        type: 'vbox'
    },
    closable: false,
    title: 'Importing ...',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'label',
                    flex: 1,
                    text: 'Importing dataset in progress:'
                },
                {
                    xtype: 'progressbar',
                    itemId: 'pb-complition',
                    animate: true,
                    value: 0.4
                }
            ],
            listeners: {
                afterrender: {
                    fn: me.onWindowAfterRender,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onWindowAfterRender: function(component, eOpts) {
        var me = this;

        var id = -1;
        id = window.setInterval(function() {
            var req = Ext.ComponentQuery.query("[itemId=view-main]")[0].createRequest();
            req.onreadystatechange = function() {
                if (req.readyState != 4) {
                    return; // Not there yet
                }
                if (req.status != 200) {
                    // Handle request failure here...
                    if (id != -1) {
                        window.clearInterval(id);
                    }
                    return;
                }
                // Request successful, read the response
                var results = JSON.parse(req.responseText);
                if (results[0] != results[1]) {
                    var progress = results[0] / results[1];
                    var percent = progress * 100.0;
                    me.down("[itemId=pb-complition]").updateProgress(progress, percent.toFixed(2) + "%", true);
                }
                else {
                    me.down("[itemId=pb-complition]").updateProgress(1, "Done", true);
                    if (id != -1) {
                        window.clearInterval(id);
                    }
                }
            }

            req.open("GET", "/import-progress", true);
            req.setRequestHeader("Content-Type", "application/json");
            req.send();
        }, 300);


    }

});
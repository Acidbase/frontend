/*
 * File: ui/store/SingleEntityStore.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Acidbase.store.SingleEntityStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Acidbase.model.single_entity'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Acidbase.model.single_entity',
            storeId: 'SingleEntityStore',
            data: [
                
            ],
            proxy: {
                type: 'rest',
                extraParams: {
                    entity_id: 1
                },
                idParam: 'entity_id',
                url: '/entity',
                writer: {
                    type: 'json'
                },
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});
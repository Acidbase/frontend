/*
 * File: ui/model/entity_info.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Acidbase.model.entity_info', {
    extend: 'Ext.data.Model',

    idProperty: 'entity_id',

    fields: [
        {
            name: 'entity_id',
            type: 'int'
        },
        {
            name: 'revision_id',
            type: 'int'
        },
        {
            name: 'entity_type',
            type: 'string'
        },
        {
            convert: function(v, rec) {
                return new Date(v);
            },
            serialize: function(value, record) {
                return value.getTime();
            },
            name: 'create_date',
            type: 'date'
        },
        {
            convert: function(v, rec) {
                return new Date(v);
            },
            serialize: function(value, record) {
                return value.getTime();
            },
            name: 'revise_date',
            type: 'date'
        },
        {
            name: 'creator',
            type: 'int'
        },
        {
            name: 'reviser',
            type: 'int'
        },
        {
            name: 'revision_state',
            type: 'string'
        },
        {
            convert: function(v, rec) {
                if (typeof v == "string" && v != "") {
                    return JSON.parse(v);
                }
                else {
                    return v;
                }
            },
            serialize: function(value, record) {
                return JSON.stringify(value);
            },
            name: 'fields',
            type: 'auto'
        },
        {
            name: 'data',
            type: 'auto'
        }
    ],

    proxy: {
        type: 'rest',
        idParam: 'entity_id',
        url: '/entity',
        writer: {
            type: 'json'
        }
    }
});
package org.acidbase.frontend;

import org.acidbase.data.ServerInfo;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class SearchController
{
    @Autowired
    private Client es_client;

    @Autowired
    private ServerInfo serverInfo;

    @RequestMapping(value = "/search/{index_name}", method = RequestMethod.GET)
    public String searchIndex(@PathVariable String index_name, @RequestParam Optional<String> phrase, @RequestParam int start, @RequestParam int limit)
    {
        SearchResponse response = es_client.prepareSearch(serverInfo.getDatabase() + "_" + index_name)
            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
            .setQuery(QueryBuilders.queryStringQuery(phrase.isPresent() && !phrase.get().equals("") ? phrase.get() : "*:*"))
            .setFrom(start).setSize(limit)
            .execute()
            .actionGet();

        SearchHits hits = response.getHits();
        long total = hits.totalHits();
        JSONArray entities = new JSONArray();
        hits.forEach(searchHitFields -> entities.add(new JSONObject(searchHitFields.sourceAsMap())));

        JSONObject result = new JSONObject();
        result.put("data", entities);
        result.put("total", total > 10000 ? 10000 : total);

        return result.toString();
    }
}

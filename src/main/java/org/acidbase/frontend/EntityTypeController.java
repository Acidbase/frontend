package org.acidbase.frontend;

import org.json.simple.parser.ParseException;
import org.acidbase.data.EntityType;
import org.acidbase.data.mapper.EntityTypeMapper;
import org.acidbase.data.IndexStructureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

@RestController
public class EntityTypeController
{
    @Autowired
    private EntityTypeMapper entityTypeMapper;

    @RequestMapping(value = "/entity-type", method = RequestMethod.GET)
    public Collection<EntityType> getEntityTypeList()
        throws ParseException, IndexStructureException, SQLException
    {
        return entityTypeMapper.getEntityType();
    }

    @RequestMapping(value = {"/entity-type", "/entity-type/{name}"}, method = RequestMethod.POST)
    public EntityType addEntityType(@PathVariable Optional<String> name, @RequestBody EntityType input)
        throws ParseException, IndexStructureException, SQLException
    {
        entityTypeMapper.addEntityType(input);
        return input;
    }

    @RequestMapping(value = "/entity-type/{name}", method = RequestMethod.PUT)
    public void editEntityType(@PathVariable String name, @RequestBody EntityType input)
        throws ParseException, IndexStructureException, SQLException
    {
        entityTypeMapper.updateEntityType(name, input);
    }

    @RequestMapping(value = "/entity-type/{name}", method = RequestMethod.DELETE)
    public void deleteEntityType(@PathVariable String name)
        throws ParseException, IndexStructureException, SQLException
    {
        entityTypeMapper.removeEntityType(name);
    }
}

package org.acidbase.frontend;

import org.acidbase.data.Command;
import org.acidbase.data.mapper.Mapper;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.SQLException;

@WebListener
public class ContextListener implements ServletContextListener
{
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent)
    {
        Mapper.registerCommand(Mapper.ConnectionEvent.AFTER_CONNECT, "enable external indexing", new Command(1) {
            @Override
            public void execute(Mapper mapper)
                    throws SQLException
            {
                mapper.query("SELECT set_config('acidbase.external_indexing', 'true', false);");
            }
        });

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {

    }
}

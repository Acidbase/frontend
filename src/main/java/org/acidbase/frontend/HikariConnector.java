package org.acidbase.frontend;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.acidbase.data.ServerInfo;
import org.acidbase.data.mapper.Connector;

import java.sql.Connection;
import java.sql.SQLException;

public class HikariConnector implements Connector
{
    private ServerInfo serverInfo;
    private HikariDataSource ds;

    public HikariConnector(ServerInfo serverInfo, HikariConfig config)
    {
        this.serverInfo = serverInfo;
        ds = new HikariDataSource(config);
    }

    @Override
    public String getIdentifier()
    {
        return serverInfo.getUrl();
    }

    @Override
    public Connection newConnection()
            throws SQLException
    {
        return ds.getConnection();
    }

    @Override
    public void close()
    {
        ds.close();
    }

    @Override
    public boolean isClosed() {
        return ds.isClosed();
    }
}

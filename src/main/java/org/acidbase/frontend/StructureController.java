package org.acidbase.frontend;

import org.acidbase.data.IndexStructure;
import org.acidbase.data.IndexStructureException;
import org.acidbase.data.mapper.IndexStructureMapper;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RestController
public class StructureController
{
    @Autowired
    private IndexStructureMapper indexStructureMapper;

    @RequestMapping(value = "/structure", method = RequestMethod.GET)
    public Collection<IndexStructure> getIndexStructureList()
        throws ParseException, IndexStructureException, SQLException
    {
        Map<String, IndexStructure> indices = indexStructureMapper.getIndexStructure();
        List<IndexStructure> structures = new ArrayList<>();
        for (Map.Entry<String, IndexStructure> entry : indices.entrySet()) {
            structures.add(entry.getValue());
        }
        return structures;
    }

    @RequestMapping(value = "/structure/{name}", method = RequestMethod.POST)
    public IndexStructure addStructure(@RequestBody IndexStructure input)
        throws ParseException, IndexStructureException, SQLException
    {
        indexStructureMapper.addIndexStructure(input.name, true, null, input.readingStructure, input.writingStructure);
        return input;
    }

    @RequestMapping(value = "/structure/{name}", method = RequestMethod.PUT)
    public void editStructure(@PathVariable String name, @RequestBody IndexStructure input)
        throws ParseException, IndexStructureException, SQLException
    {
        indexStructureMapper.updateIndexStructure(name, input.name, true, null, input.readingStructure, input.writingStructure);
    }

    @RequestMapping(value = "/structure/{name}", method = RequestMethod.DELETE)
    public void deleteStructure(@PathVariable String name)
        throws ParseException, IndexStructureException, SQLException
    {
        indexStructureMapper.removeIndexStructure(name);
    }
}

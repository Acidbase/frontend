package org.acidbase.frontend;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.acidbase.data.*;
import org.acidbase.data.mapper.EntityMapper;
import org.acidbase.data.mapper.EntityTypeMapper;
import org.acidbase.data.mapper.IndexStructureMapper;
import org.acidbase.data.mapper.Mapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class EntityController
{
    @Autowired
    private EntityMapper entityMapper;
    @Autowired
    private EntityTypeMapper entityTypeMapper;
    @Autowired
    private IndexStructureMapper indexStructureMapper;
    @Autowired
    private Config config;
    @Resource(name = "Profiles")
    private Map<String, Long> profileTimestampsl;

    @RequestMapping(value = "/entity", method = RequestMethod.GET)
    public JSONObject getEntityList(@RequestParam Optional<String> entity_type, @RequestParam Optional<Long> page, @RequestParam Optional<Long> start, @RequestParam Optional<Long> limit)
        throws SQLException
    {
        List<EntityInfo> lei = entityMapper.listEntities(
            entity_type.isPresent() ? entity_type.get() : null,
            start.isPresent() ? start.get() : null,
            limit.isPresent() ? limit.get() : null
        );
        JSONObject collection = new JSONObject();
        collection.put("data", lei);
        collection.put("total", entityMapper.countEntities(
            entity_type.isPresent() ? entity_type.get() : null
        ));
        return collection;
    }

    @RequestMapping(value = "/entity/{entity_id}", method = RequestMethod.GET)
    public EntityInfo getEntity(@PathVariable("entity_id") Long entity_id)
        throws SQLException
    {
        EntityInfo ei = entityMapper.getLatestEntityInfoByRevisionId(entity_id);
        return ei;
    }

    @RequestMapping(value = "/entity/{structure}/{entity_id}", method = RequestMethod.GET)
    public ResponseEntity getStructuredEntity(@PathVariable String structure, @PathVariable("entity_id") Long entity_id)
        throws SQLException, IndexStructureException, ParseException
    {
        return ResponseEntity.ok(entityMapper.getEntity(entity_id, indexStructureMapper.getIndexStructure(structure).readingStructure, Mapper.ExtractionState.LatestActiveOrRemoved, Mapper.LockType.NoLock));
    }

    @RequestMapping(value = "/entity/{entity_id}", method = RequestMethod.DELETE)
    public void deleteEntity(@PathVariable("entity_id") Long entity_id)
        throws SQLException
    {
        entityMapper.deleteEntity(entity_id, 1l);
    }

    @RequestMapping(value = "/revision/{revision_id}", method = RequestMethod.GET)
    public Entity getRevision(@PathVariable("revision_id") Long revision_id)
        throws SQLException, IndexStructureException, ParseException
    {
        return null;
        /*
        String jsonStructure = entityMapper.getObjectStructure(revision_id);
        IndexStructure structure = EntityExtraction.parse(jsonStructure);
        return entityMapper.getRevision(revision_id, structure, Mapper.LockType.NoLock);*/
    }

    @RequestMapping(value = { "/revision", "/revision/{revision_id}" }, method = { RequestMethod.PUT, RequestMethod.POST})
    public Entity addRevision(@PathVariable("revision_id") Optional<Long> revision_id, @RequestBody Entity entity)
        throws SQLException, IndexStructureException, ParseException
    {
        EntityInfo ei = null;
        if (revision_id.isPresent() && revision_id.get() > 0) {
            ei = entityMapper.getLatestEntityInfoByRevisionId(revision_id.get());
            if (ei != null) {
                ei = entityMapper.storeEntity(entity.entity_type, entity, 1l, ei.entity_id);
            }
        }
        else {
            ei = entityMapper.storeEntity(entity.entity_type, entity, 1l);
        }
        entity.entity_id = ei.entity_id;
        entity.revision_id = ei.revision_id;
        entity.creator = ei.creator;
        entity.reviser = ei.reviser;
        entity.create_date = ei.create_date;
        entity.revise_date = ei.revise_date;
        return entity;
    }

    private long totalRecords = 0;
    private long recordCounter = 0;
    private static final long transactionChunkSize = 100;
    private boolean abort = false;
    private boolean done_importing = false;
    @RequestMapping(value = "/entity/import-ds2", method = RequestMethod.POST)
    public void importDs2(
            @RequestParam(required = false, name = "structure") Boolean createStructure,
            @RequestParam(required = false, name = "category") Boolean importProductCategory,
            @RequestParam(required = false, name = "product") Boolean importProduct,
            @RequestParam(required = false, name = "customer") Boolean importCustomer,
            @RequestParam(required = false, name = "order") Boolean importOrder)
        throws SQLException, IOException, IndexStructureException, ParseException, InterruptedException
    {
        abort = false;
        done_importing = false;
        totalRecords = 0;
        recordCounter = 0;

        Connection con = entityMapper.getConnection();
        Integer key;

        if (createStructure != null && createStructure) {
            key = entityTypeMapper.beginTransaction();
            saveEntityType("{\"name\":\"Category\",\"fields\":{\"name\":\"string\"}}");
            saveEntityType("{\"name\":\"Product\",\"fields\":{\"title\":\"string\",\"actor\":\"string\",\"price\":\"double\",\"special\":\"boolean\"}}");
            saveEntityType("{\"name\":\"Customer\",\"fields\":{\"first name\":\"string\",\"last name\":\"string\",\"address1\":\"string\",\"address2\":\"string\",\"city\":\"string\",\"state\":\"string\",\"zip\":\"string\",\"country\":\"string\",\"region\":\"string\",\"email\":\"string\",\"phone\":\"string\",\"credit card type\":\"integer\",\"credit card\":\"string\",\"credit card expiration\":\"string\",\"username\":\"string\",\"password\":\"string\",\"age\":\"integer\",\"income\":\"long\",\"gender\":\"string\"}}");
            saveEntityType("{\"name\":\"Order\",\"fields\":{\"order date\":\"date\",\"net amount\":\"double\",\"tax\":\"double\",\"total amount\":\"double\"}}");
            entityTypeMapper.commit(key);

            key = indexStructureMapper.beginTransaction();
            saveIndexStructure("{\"name\":\"product_list\", \"index\": true,\"writingStructure\":{\"revision_state\":\"LatestActive\",\"entity_type\":\"Product\",\"fields\":{\"actor\":\"string\",\"special\":\"boolean\",\"price\":\"double\",\"title\":\"string\"},\"collections\":{\"Category\":{\"revision_state\":\"Latest\",\"entity_type\":\"Category\",\"fields\":{\"name\":\"string\"},\"collections\":{}},\"Inventory\":{\"revision_state\":\"NotAnEntity\",\"entity_type\":\"\",\"fields\":{\"stock\":\"long\",\"sales\":\"long\"},\"collections\":{}}}},\"readingStructure\":{\"revision_state\":\"LatestActive\",\"entity_type\":\"Product\",\"fields\":[\"actor\",\"special\",\"price\",\"title\"],\"collections\":{\"Category\":{\"revision_state\":\"Latest\",\"entity_type\":\"Category\",\"fields\":[\"name\"],\"collections\":{}},\"Inventory\":{\"revision_state\":\"NotAnEntity\",\"entity_type\":\"\",\"fields\":[\"stock\",\"sales\"],\"collections\":{}}}}}");
            saveIndexStructure("{\"name\":\"Customer\", \"index\": true,\"writingStructure\":{\"revision_state\":\"LatestActive\",\"entity_type\":\"Customer\",\"fields\":{\"zip\":\"string\",\"income\":\"long\",\"country\":\"string\",\"gender\":\"string\",\"city\":\"string\",\"address2\":\"string\",\"credit card type\":\"integer\",\"address 1\":\"string\",\"credit card expiration\":\"string\",\"last name\":\"string\",\"first name\":\"string\",\"phone\":\"string\",\"state\":\"string\",\"credit card\":\"string\",\"region\":\"string\",\"age\":\"integer\",\"email\":\"string\",\"username\":\"string\",\"password\":\"string\"},\"collections\":{}}, \"readingStructure\":{\"revision_state\":\"LatestActive\",\"entity_type\":\"Customer\",\"fields\":[\"zip\",\"income\",\"country\",\"gender\",\"city\",\"address2\",\"credit card type\",\"address 1\",\"credit card expiration\",\"last name\",\"first name\",\"phone\",\"state\",\"credit card\",\"region\",\"age\",\"email\",\"username\",\"password\"],\"collections\":{}}}");
            saveIndexStructure("{\"name\":\"order\", \"index\": true,\"writingStructure\":{\"revision_state\":\"LatestActive\",\"entity_type\":\"Order\",\"fields\":{\"net amount\":\"double\",\"order date\":\"date\",\"tax\":\"double\",\"total amount\":\"double\"},\"collections\":{\"Order lines\":{\"revision_state\":\"Revision\",\"entity_type\":\"Product\",\"fields\":{\"actor\":\"string\",\"price\":\"double\",\"Field 2special\":\"boolean\",\"title\":\"string\"},\"collections\":{\"Category\":{\"revision_state\":\"Latest\",\"entity_type\":\"Category\",\"fields\":{\"name\":\"string\"},\"collections\":{}}}},\"Customer\":{\"revision_state\":\"Latest\",\"entity_type\":\"Customer\",\"fields\":{\"zip\":\"string\",\"income\":\"long\",\"country\":\"string\",\"gender\":\"string\",\"city\":\"string\",\"address2\":\"string\",\"credit card type\":\"integer\",\"address1\":\"string\",\"credit card expiration\":\"string\",\"password\":\"string\",\"last name\":\"string\",\"first name\":\"string\",\"phone\":\"string\",\"state\":\"string\",\"credit card\":\"string\",\"region\":\"string\",\"age\":\"integer\",\"email\":\"string\",\"username\":\"string\"},\"collections\":{}}}}, \"readingStructure\":{\"revision_state\":\"LatestActive\",\"entity_type\":\"Order\",\"fields\":[\"net amount\",\"order date\",\"tax\",\"total amount\"],\"collections\":{\"Order lines\":{\"revision_state\":\"Revision\",\"entity_type\":\"Product\",\"fields\":[\"actor\",\"price\",\"Field 2special\",\"title\"],\"collections\":{\"Category\":{\"revision_state\":\"Latest\",\"entity_type\":\"Category\",\"fields\":[\"name\"],\"collections\":{}}}},\"Customer\":{\"revision_state\":\"Latest\",\"entity_type\":\"Customer\",\"fields\":[\"zip\",\"income\",\"country\",\"gender\",\"city\",\"address2\",\"credit card type\",\"address1\",\"credit card expiration\",\"password\",\"last name\",\"first name\",\"phone\",\"state\",\"credit card\",\"region\",\"age\",\"email\",\"username\"],\"collections\":{}}}}}");
            indexStructureMapper.commit(key);
        }

        java.lang.Thread.sleep(1000);

        key = entityMapper.beginTransaction();
        try {
            if (importProductCategory != null && importProductCategory) {
                totalRecords += countRecords("SELECT COUNT(*) FROM categories;");
            }
            if (importProduct != null && importProduct) {
                totalRecords += countRecords("SELECT COUNT(*) FROM products p INNER JOIN inventory i ON (p.prod_id = i.prod_id);");
            }
            if (importCustomer != null && importCustomer) {
                totalRecords += countRecords("SELECT COUNT(*) FROM customers;");
            }
            if (importOrder != null && importOrder) {
                totalRecords += countRecords("SELECT COUNT(*) FROM orders o INNER JOIN orderlines l ON (o.orderid = l.orderid);");
            }


            //Product categories
            if (importProductCategory != null && importProductCategory) {
                Statement stmt = con.createStatement();
                stmt.executeUpdate("DROP TABLE IF EXISTS cat_rel;");
                stmt.executeUpdate("CREATE TABLE cat_rel (id integer NOT NULL, revision_id bigint NOT NULL, CONSTRAINT cat_rel_pri PRIMARY KEY (id));");
                PreparedStatement pst = con.prepareStatement("INSERT INTO cat_rel VALUES (?, ?);");
                ResultSet rs = stmt.executeQuery("SELECT category, categoryname FROM categories;");
                while (rs.next()) {
                    int cid = rs.getInt(1);
                    String cn = rs.getString(2);

                    Entity data = new Entity();
                    data.fields.put("name", cn);
                    EntityInfo ei = entityMapper.storeEntity("Category", data, 1l);
                    pst.setInt(1, cid);
                    pst.setLong(2, ei.revision_id);
                    pst.executeUpdate();

                    recordCounter++;
                    if (abort) {
                        entityMapper.rollback(key);
                        return;
                    }
                    else if (recordCounter % transactionChunkSize == 0) {
                        entityMapper.commit(key);
                        key = entityMapper.beginTransaction();
                    }
                }
                rs.close();
                stmt.close();
                pst.close();
            }

            //Products
            if (importProduct != null && importProduct) {
                Statement stmt = con.createStatement();
                stmt.executeUpdate("DROP TABLE IF EXISTS prd_rel;");
                stmt.executeUpdate("CREATE TABLE prd_rel (id integer NOT NULL, revision_id bigint NOT NULL, CONSTRAINT prd_rel_pri PRIMARY KEY (id));");
                PreparedStatement pst = con.prepareStatement("INSERT INTO prd_rel VALUES (?, ?);");
                ResultSet rs = stmt.executeQuery("SELECT p.prod_id, p.title, p.actor, p.price, p.special, c.revision_id, i.quan_in_stock, i.sales FROM products p INNER JOIN cat_rel c ON (p.category = c.id) INNER JOIN inventory i ON (p.prod_id = i.prod_id);");
                if (config.getApp_profile()) {
                    profileTimestampsl.put("start_product", System.currentTimeMillis());
                }
                while (rs.next()) {
                    long pid = rs.getLong(1);
                    String title = rs.getString(2);
                    String actor = rs.getString(3);
                    double price = rs.getDouble(4);
                    boolean special = rs.getBoolean(5);
                    long category_revision_id = rs.getLong(6);
                    long stock = rs.getLong(7);
                    long sales = rs.getLong(8);

                    Entity data = new Entity();
                    {
                        data.fields.put("title", title);
                        data.fields.put("actor", actor);
                        data.fields.put("price", price);
                        data.fields.put("special", special);

                        //Category
                        {
                            List<Entity> collection = new ArrayList<>();
                            Entity category = new Entity();
                            category.revision_id = category_revision_id;
                            collection.add(category);
                            data.collections.put("Category", collection);
                        }

                        //Inventory
                        {
                            List<Entity> collection = new ArrayList<>();
                            Entity inventory = new Entity();
                            inventory.fields.put("stock", stock);
                            inventory.fields.put("sales", sales);
                            collection.add(inventory);
                            data.collections.put("Inventory", collection);
                        }
                    }
                    EntityInfo ei = entityMapper.storeEntity("Product", data, 1l);
                    pst.setLong(1, pid);
                    pst.setLong(2, ei.revision_id);
                    pst.executeUpdate();

                    recordCounter++;
                    if (abort) {
                        entityMapper.rollback(key);
                        return;
                    }
                    else if (recordCounter % transactionChunkSize == 0) {
                        entityMapper.commit(key);
                        key = entityMapper.beginTransaction();
                    }
                }
                rs.close();
                stmt.close();
                pst.close();
                if (config.getApp_profile()) {
                    profileTimestampsl.put("end_product", System.currentTimeMillis());
                }
            }

            //Customers
            if (importCustomer != null && importCustomer) {
                Statement stmt = con.createStatement();
                stmt.executeUpdate("DROP TABLE IF EXISTS cus_rel;");
                stmt.executeUpdate("CREATE TABLE cus_rel (id integer NOT NULL, revision_id bigint NOT NULL, CONSTRAINT cus_rel_pri PRIMARY KEY (id));");
                PreparedStatement pst = con.prepareStatement("INSERT INTO cus_rel VALUES (?, ?);");
                ResultSet rs = stmt.executeQuery("SELECT customerid, firstname, lastname, address1, address2, city, state, zip, country, region, email, phone, creditcardtype, creditcard, creditcardexpiration, username, password, age, income, gender FROM customers;");
                if (config.getApp_profile()) {
                    profileTimestampsl.put("start_customer", System.currentTimeMillis());
                }
                while (rs.next()) {
                    long customerid = rs.getLong(1);
                    String firstname = rs.getString(2);
                    String lastname = rs.getString(3);
                    String address1 = rs.getString(4);
                    String address2 = rs.getString(5);
                    String city = rs.getString(6);
                    String state = rs.getString(7);
                    String zip = rs.getString(8);
                    String country = rs.getString(9);
                    int region = rs.getInt(10);
                    String email = rs.getString(11);
                    String phone = rs.getString(12);
                    int creditcardtype = rs.getInt(13);
                    String creditcard = rs.getString(14);
                    String creditcardexpiration = rs.getString(15);
                    String username = rs.getString(16);
                    String password = rs.getString(17);
                    int age = rs.getInt(18);
                    long income = rs.getLong(19);
                    String gender = rs.getString(20);

                    Entity data = new Entity();
                    {
                        data.fields.put("first name", firstname);
                        data.fields.put("last name", lastname);
                        data.fields.put("address1", address1);
                        data.fields.put("address2", address2);
                        data.fields.put("city", city);
                        data.fields.put("state", state);
                        data.fields.put("zip", zip);
                        data.fields.put("country", country);
                        data.fields.put("region", region);
                        data.fields.put("email", email);
                        data.fields.put("phone", phone);
                        data.fields.put("credit card type", creditcardtype);
                        data.fields.put("credit card", creditcard);
                        data.fields.put("credit card expiration", creditcardexpiration);
                        data.fields.put("username", username);
                        data.fields.put("password", password);
                        data.fields.put("age", age);
                        data.fields.put("income", income);
                        data.fields.put("gender", gender);
                    }
                    EntityInfo ei = entityMapper.storeEntity("Customer", data, 1l);
                    pst.setLong(1, customerid);
                    pst.setLong(2, ei.revision_id);
                    pst.executeUpdate();

                    recordCounter++;
                    if (abort) {
                        entityMapper.rollback(key);
                        return;
                    }
                    else if (recordCounter % transactionChunkSize == 0) {
                        entityMapper.commit(key);
                        key = entityMapper.beginTransaction();
                    }
                }
                rs.close();
                stmt.close();
                pst.close();
                if (config.getApp_profile()) {
                    profileTimestampsl.put("end_customer", System.currentTimeMillis());
                }
            }

            //Orders
            if (importOrder != null && importOrder) {
                Statement stmt = con.createStatement();
                stmt.executeUpdate("DROP TABLE IF EXISTS ordr_rel;");
                stmt.executeUpdate("CREATE TABLE ordr_rel (id integer NOT NULL, revision_id bigint NOT NULL, CONSTRAINT ordr_rel_pri PRIMARY KEY (id));");
                PreparedStatement pst = con.prepareStatement("INSERT INTO ordr_rel VALUES (?, ?);");
                Statement stmt2 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt2.executeQuery("SELECT o.orderid, o.orderdate, o.netamount, o.tax, o.totalamount, l.quantity, l.orderdate, c.revision_id AS customer_revision_id, p.revision_id AS product_revision_id FROM orders o INNER JOIN orderlines l ON (o.orderid = l.orderid) INNER JOIN cus_rel c ON (o.customerid = c.id) INNER JOIN prd_rel p ON (l.prod_id = p.id) ORDER BY o.orderid, l.orderlineid;");
                if (config.getApp_profile()) {
                    profileTimestampsl.put("start_order", System.currentTimeMillis());
                }
                while (rs.next()) {
                    long orderid = rs.getLong(1);
                    Date orderdate = rs.getDate(2);
                    double netamount = rs.getDouble(3);
                    double tax = rs.getDouble(4);
                    double totalamount = rs.getDouble(5);
                    long customer_revision_id = rs.getLong(8);

                    Entity data = new Entity();
                    {
                        data.fields.put("order date", orderdate.toString());
                        data.fields.put("net amount", netamount);
                        data.fields.put("tax", tax);
                        data.fields.put("total amount", totalamount);

                        //Customer
                        {
                            List<Entity> collection = new ArrayList<>();
                            Entity customer = new Entity();
                            customer.revision_id = customer_revision_id;
                            collection.add(customer);
                            data.collections.put("Customer", collection);
                        }

                        //Inventory
                        {
                            List<Entity> collection = new ArrayList<>();
                            while (rs.next()) {
                                if (rs.getLong(1) != orderid) {
                                    rs.previous();
                                    break;
                                }
                                int quantity = rs.getInt(6);
                                Date lineorderdate = rs.getDate(7);
                                long product_revision_id = rs.getLong(9);

                                Entity orderline = new Entity();
                                orderline.fields.put("quantity", quantity);
                                orderline.fields.put("order date", lineorderdate.toString());
                                orderline.revision_id = product_revision_id;
                                collection.add(orderline);

                                recordCounter++;
                                if (abort) {
                                    entityMapper.rollback(key);
                                    return;
                                }
                                else if (recordCounter % transactionChunkSize == 0) {
                                    entityMapper.commit(key);
                                    key = entityMapper.beginTransaction();
                                }
                            }
                            data.collections.put("Order lines", collection);
                        }
                    }

                    EntityInfo ei = entityMapper.storeEntity("Order", data, 1l);
                    pst.setLong(1, orderid);
                    pst.setLong(2, ei.revision_id);
                    pst.executeUpdate();

                    recordCounter++;
                    if (abort) {
                        entityMapper.rollback(key);
                        return;
                    }
                    else if (recordCounter % transactionChunkSize == 0) {
                        entityMapper.commit(key);
                        key = entityMapper.beginTransaction();
                    }
                }
                rs.close();
                stmt2.close();
                stmt.close();
                pst.close();

                if (config.getApp_profile()) {
                    profileTimestampsl.put("end_order", System.currentTimeMillis());
                }
            }

            //Commit all
            entityMapper.commit(key);
            done_importing = true;
        }
        catch (Exception ex) {
            entityMapper.rollback(key);
            throw ex;
        }
        finally {
            if (importOrder != null && importOrder) {
                Statement stmt = con.createStatement();
                stmt.executeUpdate("DROP TABLE IF EXISTS cat_rel;");
                stmt.executeUpdate("DROP TABLE IF EXISTS prd_rel;");
                stmt.executeUpdate("DROP TABLE IF EXISTS cus_rel;");
                stmt.executeUpdate("DROP TABLE IF EXISTS ordr_rel;");
            }
        }
    }

    @RequestMapping(value = "/profiling", method = RequestMethod.GET)
    public void writeDownProfiling()
    {
        Map<String, Long> profileTimestamps = profileTimestampsl;
        BufferedWriter writer = null;
        try {
            File profileFile = new File(config.getApp_profile_output());
            writer = new BufferedWriter(new FileWriter(profileFile));
            for (Map.Entry<String, Long> entry : profileTimestamps.entrySet()) {
                writer.write(entry.getKey() + " " + entry.getValue().toString() + "\n");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            }
            catch (Exception ignored) {}
        }
    }

    @RequestMapping(value = "/import-progress", method = RequestMethod.GET)
    public JSONObject getProgress()
    {
        JSONObject report = new JSONObject();
        report.put("recordCounter", recordCounter);
        report.put("totalRecords", totalRecords);
        report.put("abort", abort);
        report.put("doneImporting", done_importing);
        return report;
    }

    @RequestMapping(value = "/import-abort", method = RequestMethod.POST)
    public void abortImport()
    {
        abort = true;
    }

    private void saveEntityType(String json)
        throws IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        EntityType input = objectMapper.readValue(json, EntityType.class);
        boolean added = false;
        try {
            added = entityTypeMapper.addEntityType(input);
        }
        catch (Exception ex) {
            added = false;
        }
        if (!added) {
            try {
                entityTypeMapper.updateEntityType(input.getName(), input);
            }
            catch (Exception e) {}
        }
    }

    private void saveIndexStructure(String json)
        throws IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        IndexStructure input = objectMapper.readValue(json, IndexStructure.class);
        boolean added = false;
        try {
            added = indexStructureMapper.addIndexStructure(input.name, true, null, input.readingStructure, input.writingStructure);
        }
        catch (Exception ex) {
            added = false;
        }
        if (!added) {
            try {
                indexStructureMapper.updateIndexStructure(input.name, input.name, true, null, input.readingStructure, input.writingStructure);
            }
            catch (Exception e) {}
        }
    }

    private long countRecords(String query)
        throws SQLException
    {
        Connection con = entityMapper.getConnection();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        return rs.getLong(1);
    }
}

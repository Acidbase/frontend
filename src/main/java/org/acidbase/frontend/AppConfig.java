package org.acidbase.frontend;

import com.zaxxer.hikari.HikariConfig;
import org.acidbase.data.ServerInfo;
import org.acidbase.data.mapper.EntityMapper;
import org.acidbase.data.mapper.EntityTypeMapper;
import org.acidbase.data.mapper.IndexStructureMapper;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import javax.annotation.PreDestroy;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Configuration
@PropertySource("/config.properties")
@ComponentScan({"org.acidbase.frontend"})
@ServletComponentScan
public class AppConfig implements ApplicationContextAware {
    private ApplicationContext appContext;
    @Autowired
    private Environment env;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
        throws BeansException
    {
        this.appContext = applicationContext;
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public ServerInfo getDbServerInfo() {
        return new ServerInfo(
                getDb_protocol(),
                getDb_type(),
                getDb_host(),
                getDb_port(),
                getDb_database(),
                getDb_username(),
                getDb_password()
        );
    }

    static Map<String, HikariConnector> connectors = new HashMap<>();

    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public HikariConnector getConnector()
    {
        ServerInfo serverInfo = getDbServerInfo();
        if (!connectors.containsKey(serverInfo.getUrl())) {
            HikariConfig config = new HikariConfig();

            config.setJdbcUrl(serverInfo.getUrl());
            config.setUsername(serverInfo.getUsername());
            config.setPassword(serverInfo.getPassword());
            config.setConnectionTimeout(Long.parseLong(env.getProperty("cp.connectionTimeout")));
            config.setIdleTimeout(Long.parseLong(env.getProperty("cp.idleTimeout")));
            config.setMaxLifetime(Long.parseLong(env.getProperty("cp.maxLifetime")));
            config.setMinimumIdle(Integer.parseInt(env.getProperty("cp.minimumIdle")));
            config.setMaximumPoolSize(Integer.parseInt(env.getProperty("cp.maximumPoolSize")));

            connectors.put(serverInfo.getUrl(), new HikariConnector(serverInfo, config));
        }

        return connectors.get(serverInfo.getUrl());
    }

    @Bean(destroyMethod = "close")
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public EntityMapper getEntityMapper()
    {
        return new EntityMapper(getConnector());
    }

    @Bean(destroyMethod = "close")
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public EntityTypeMapper getDefaultEntityTypeMapper()
    {
        return new EntityTypeMapper(getConnector());
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public IndexStructureMapper getDefaultIndexStructureMapper()
        throws ExecutionException
    {
        return new IndexStructureMapper(getConnector());
    }

    @Bean
    public Client getElasticSearchClient()
        throws UnknownHostException
    {
        Settings es_settings = Settings
            .settingsBuilder()
            .put("cluster.name", getEs_cluster_name())
            .put("node.name", getEs_node_name())
            .build();
        return TransportClient
            .builder()
            .settings(es_settings)
            .build()
            .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(getEs_host()), getEs_port()));
    }

    @Bean
    public Config getConfig()
    {
        Config cfg = new Config();
        cfg.setApp_profile(env.getProperty("app.profile").trim().equals("1"));
        cfg.setApp_profile_output(env.getProperty("app.profile_output").trim());
        return cfg;
    }

    @Bean(name = "Profiles")
    public Map<String, Long> getProfiles()
    {
        return new HashMap<>();
    }

    @PreDestroy
    public void preDestroy()
    {
        Config cfg = getConfig();
        if (cfg.getApp_profile()) {
            Map<String, Long> profileTimestamps = getProfiles();
            BufferedWriter writer = null;
            try {
                File profileFile = new File(cfg.getApp_profile_output());
                writer = new BufferedWriter(new FileWriter(profileFile));
                for (Map.Entry<String, Long> entry : profileTimestamps.entrySet()) {
                    writer.write(entry.getKey() + " " + entry.getValue().toString() + "\n");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                try {
                    if (writer != null) {
                        writer.close();
                    }
                }
                catch (Exception ignored) {}
            }
        }
    }

    //region Private configurations

    private String getDb_protocol()
    {
        return env.getProperty("db.protocol").trim();
    }

    private String getDb_type()
    {
        return env.getProperty("db.type").trim();
    }

    private String getDb_host()
    {
        return env.getProperty("db.host").trim();
    }

    private int getDb_port()
    {
        return Integer.parseInt(env.getProperty("db.port").trim());
    }

    private String getDb_database()
    {
        return env.getProperty("db.database").trim();
    }

    private String getDb_username()
    {
        return env.getProperty("db.username").trim();
    }

    private String getDb_password()
    {
        return env.getProperty("db.password").trim();
    }



    private String getEs_host()
    {
        return env.getProperty("es.host").trim();
    }

    private int getEs_port()
    {
        return Integer.parseInt(env.getProperty("es.port").trim());
    }

    private String getEs_cluster_name()
    {
        return env.getProperty("es.cluster_name").trim();
    }

    private String getEs_node_name()
    {
        return env.getProperty("es.node_name").trim();
    }

    //endregion
}

package org.acidbase.frontend;

public class Config
{
    private boolean app_profile;
    private String app_profile_output;
    private String db_url;
    private String db_username;
    private String db_password;
    private String es_host;
    private int es_port;
    private String es_cluster_name;
    private String es_node_name;

    public void setApp_profile(boolean profile)
    {
        app_profile = profile;
    }

    public boolean getApp_profile()
    {
        return app_profile;
    }

    public void setApp_profile_output(String profile_output)
    {
        app_profile_output = profile_output;
    }

    public String getApp_profile_output()
    {
        return app_profile_output;
    }

    public void setDb_url(String url)
    {
        db_url = url;
    }

    public String getDb_url()
    {
        return db_url;
    }

    public void setDb_username(String username)
    {
        db_username = username;
    }

    public String getDb_username()
    {
        return db_username;
    }

    public void setDb_password(String password)
    {
        db_password = password;
    }

    public String getDb_password()
    {
        return db_password;
    }

    public void setEs_host(String host)
    {
        es_host = host;
    }

    public String getEs_host()
    {
        return es_host;
    }

    public void setEs_port(int port)
    {
        es_port = port;
    }

    public int getEs_port()
    {
        return es_port;
    }

    public void setEs_cluster_name(String cluster_name)
    {
        es_cluster_name = cluster_name;
    }

    public String getEs_cluster_name()
    {
        return es_cluster_name;
    }

    public void setEs_node_name(String node_name)
    {
        es_node_name = node_name;
    }

    public String getEs_node_name()
    {
        return es_node_name;
    }
}
